#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

bool isOnlyDouble(const char *str) {
    char *endptr = nullptr;
    strtod(str, &endptr);

    return !(*endptr != '\0' || endptr == str);
}

double coshSequenceSum(double x, double precision) {
    double result = 1, numerator = 1, denominator = 1;
    double delta;
    int m = 0;
    do {
        numerator *= x * x;
        denominator *= (m+1)*(m+2);
        m += 2;
        delta = numerator / denominator;
        result += delta;
    } while (delta > precision);
    return result;
}

void lab4(double x, double precision) {
    double value_standard = cosh(x);
    double value_sum = coshSequenceSum(x, precision);
    cout << "p = " << precision
    << setprecision(std::numeric_limits<double>::max_digits10)
    << "\nx = " << x << "\ncosh(x) = "
    << "\n Standard function:\t " << value_standard
    << "\n Sequence sum:\t\t " << value_sum
    << "\nΔ = " << value_standard - value_sum << endl;
}


int main(int argc, char **argv) {
    srand(clock());

    double precision;
    if (argc != 2) {
        cout << "Usage: " << argv[0] << " [precision]" << endl;
        return 0;
    }
    try {
        precision = stod(argv[1]);
        if (!isOnlyDouble(argv[1])) {
            throw invalid_argument("not a double value");
        }
    } catch (invalid_argument&) {
        cerr << "Incorrect precision value" << endl;
        return -1;
    }

    double x = ((double) rand() / (RAND_MAX));
    lab4(x, precision);

    return 0;
}
